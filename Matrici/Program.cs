﻿using System;

namespace Matrici
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int[,] numere = new int[5, 5];

            for (int rand = 0; rand < numere.GetLength(0); rand++)
            {
                for (int coloana = 0; coloana < numere.GetLength(1); coloana++)
                {
                    numere[rand, coloana] = (rand + 1) * (coloana + 1);
                }

                Console.WriteLine();
            }


            Console.WriteLine(numere.Length);

            // afisam matricea, exact asa cum este definita
            for (int rand = 0; rand < numere.GetLength(0); rand++)
            {
                for (int coloana = 0; coloana < numere.GetLength(1); coloana++)
                {
                    Console.Write(numere[rand, coloana]);

                    if (coloana != numere.GetLength(1) - 1)
                        Console.Write(", ");
                }

                Console.WriteLine();
            }

            // suma intregii matrici
            int suma = 0;

            for (int rand = 0; rand < numere.GetLength(0); rand++)
            {
                for (int coloana = 0; coloana < numere.GetLength(1); coloana++)
                {
                    suma += numere[rand, coloana];
                }
            }

            Console.WriteLine("Suma matricei este: {0}", suma);

            // media numerelor din matrice
            double medieMatrice = (double) suma / (numere.GetLength(0) * numere.GetLength(1));

            Console.WriteLine("Media numerelor din matrice este: {0}", medieMatrice);

            // suma pe linii si coloane
            // vom stoca suma pentru fiecare linie/coloana, intr-un vector
            int[] sumaLinii = new int[numere.GetLength(0)];
            int[] sumaColoane = new int[numere.GetLength(1)];

            for (int rand = 0; rand < numere.GetLength(0); rand++)
            {
                int sumaLinie = 0;
                for (int coloana = 0; coloana < numere.GetLength(1); coloana++)
                {
                    sumaLinie += numere[rand, coloana];
                }

                sumaLinii[rand] = sumaLinie;
            }

            for (int coloana = 0; coloana < numere.GetLength(1); coloana++)
            {
                int sumaColoana = 0;
                for (int rand = 0; rand < numere.GetLength(0); rand++)
                {
                    sumaColoana += numere[rand, coloana];
                }

                sumaColoane[coloana] = sumaColoana;
            }

            // afisam sumele
            for (int i = 0; i < sumaLinii.Length; i++)
            {
                Console.WriteLine("Suma pe linia {0} este: {1}.", i + 1, sumaLinii[i]);
                Console.WriteLine("Valoare medie pe linia {0} este: {1}.", i + 1,
                    (double) sumaLinii[i] / numere.GetLength(0));
            }

            for (int i = 0; i < sumaColoane.Length; i++)
            {
                Console.WriteLine("Suma pe coloana {0} este: {1}.", i + 1, sumaColoane[i]);
                Console.WriteLine("Valoare medie pe coloana {0} este: {1}.", i + 1,
                    (double) sumaColoane[i] / numere.GetLength(1));
            }

            Console.ReadKey();
        }
    }
}